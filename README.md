# Acommerce Homework challenges - Magento Basic
Time: about 60 minutes recommended except step 1 and 2. <br />
Expected: Please implement all above below and then commit necessary files and push to public repository <br />
1. Install last stable Magento version in your local <br />
2. Install Product Sample package in project <br />
3. Create Custom Module Extension and install <br />
4. **Requirement**: We need to customize product detail page <br />
   &nbsp; * Remove review feature such as **rating, review tab and button** <br />
   &nbsp; * Insert message is **"Good quality product"** and use style css to make when hover this message it will change color 
             under product name or instead of review feature <br />
   ![Magento Basic](https://bitbucket.org/acommerce/homework-challenges-magento/raw/33ee680656cf4e3438952bf4fd475add6ac6c30b/PDP-Customize.png)             
5. **Requirement**: We need to keep log file after press Add to Cart button to tracking customer action with product <br />
   &nbsp; * Log file name as **product_tracking.log** <br />
   &nbsp; * Log file information <br />
   &nbsp;&nbsp;&nbsp; * Customer name, if guest user should be "Guest user" <br />
   &nbsp;&nbsp;&nbsp; * Product Name <br />
   &nbsp;&nbsp;&nbsp; * Qty <br />
Ref: https://www.mgt-commerce.com/ (Setting up magento)